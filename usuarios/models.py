# Django
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import Permission

# Create your models here.

class PerfilDeUsuario(models.Model):
    """ 
    Modelo que extiende el perfil de usuario para incluir campos extra que por defecto no contiene el modelo de usuario de Django
    """
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    segundo_nombre = models.CharField(max_length=150)
    segundo_apellido = models.CharField(max_length=150)

    def __str__(self):
        return self.usuario.username

@receiver(post_save, sender=User)
def agregar_permisos(sender, instance, created, **kwargs):
    if created:
        # Validamos que el nuevo usuario sea un usuario de tipo administrador (staff)
        if instance.is_staff:
            # Le damos permiso para consultar la lista de usuarios al nuevo administrador
            consultar_usuarios = Permission.objects.get(codename='view_user')
            instance.user_permissions.add(consultar_usuarios)

            # Le damos permiso para crear usuarios al nuevo administrador
            crear_usuarios = Permission.objects.get(codename='add_user')
            instance.user_permissions.add(crear_usuarios)

            # Le damos permiso para editar usuarios al nuevo administrador, esto es necesario por que Django lo exige para crear nuevos usuarios, es decir, no basta con 'add_user' (Lo se no tiene sentido).
            editar_usuarios = Permission.objects.get(codename='change_user')
            instance.user_permissions.add(editar_usuarios)