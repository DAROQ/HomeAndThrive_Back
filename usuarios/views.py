# Django
from django.shortcuts import render
from django.contrib.auth.models import User, Group

# Django Rest Framework
from rest_framework import viewsets
from rest_framework import permissions

# Serializadores
from .serializers import UserSerializer, GroupSerializer

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    """
    Endpoint que permite consultar y editar los usuarios del sistema
    """

    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

class GroupViewSet(viewsets.ModelViewSet):
    """
    Endpoint que permite consultar y editar los grupos (de usuarios) del sistema
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]