# Django
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin

# Formularios
from .forms import CustomUserCreationForm, CustomUserChangeForm

# Register your models here.

class CustomUserAdmin(UserAdmin):
    """
    Modelo de usuario personalizado para el sitio de administración de Django
    """
    add_form = CustomUserCreationForm

    # Definimos los campos que se mostrarán en el formulario de creación de usuarios, notese que no incluimos el campo 'username', se usará el correo como nombre de usuario
    add_fieldsets = [
        (None, {
            'classes': ('wide'),
            'fields': ('email', 'password1', 'password2', 'first_name', 'segundo_nombre', 'last_name', 'segundo_apellido')
        })
    ]

    form = CustomUserChangeForm

    # Definición de los campos del formulario de edición de usuarios para los super administradores(Superusers)
    superadmins_fieldsets = [
        (None, {
            "fields": ("password",)
        }),
        (("Información Personal"), {
            "fields": ("first_name", 'segundo_nombre', "last_name", 'segundo_apellido', "email")
        }),
        (("Permisos"), {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
        }),
        (("Fechas Importantes"), {
            "fields": ("last_login", "date_joined")
        })
    ]

    # Definición de los campos del formulario de edición de usuarios para los administradores(Staff)
    # La diferencia es que se les quita la sección de permisos para que no puedan asignarse permisos y grupos a ellos mismos, ni convertirse en superadministradores
    admins_fieldsets = [
        (None, {
            "fields": ("password",)
        }),
        (("Información Personal"), {
            "fields": ("first_name", 'segundo_nombre', "last_name", 'segundo_apellido', "email")
        }),
        (("Fechas Importantes"), {
            "fields": ("last_login", "date_joined")
        })
    ]

    def change_view(self, request, *args, **kwargs):
        """
        Este método se encarga de construir de forma dinámica el formulario de edición de usuarios, por ahora existen dos configuraciones:
        Una para los super administradores(Superusers) y otra para los administradores(Staff).
        """
        if not request.user.is_superuser:
            # Se cambia el formulario por defecto por el formulario de administradores
            try:
                self.fieldsets = self.admins_fieldsets
                response = super(CustomUserAdmin, self).change_view(request, *args, **kwargs)
            finally:
                # Se resetean los fieldsets a su valor por defecto
                self.fieldsets = CustomUserAdmin.fieldsets
            
        else:
            # Se cambia el formulario por defecto por el formulario de super administradores
            try:
                self.fieldsets = self.superadmins_fieldsets
                response = super(CustomUserAdmin, self).change_view(request, *args, **kwargs)
            finally:
                # Se resetean los fieldsets a su valor por defecto
                self.fieldsets = CustomUserAdmin.fieldsets
        
        return response
                

# Registramos nuestro modelo de usuario personalizado en el sitio de administración de Django
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)