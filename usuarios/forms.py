# Django
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms
from django.contrib.auth.models import User

# Modelos
from .models import PerfilDeUsuario

# Create your forms here.

class CustomUserCreationForm(UserCreationForm):
    """
    Formulario de creación de usuarios personalizado para incluir los campos 'Segundo nombre' y 'Segundo Apellido'.
    """
    segundo_nombre = forms.CharField(required=False, max_length=150)
    segundo_apellido = forms.CharField(max_length=150)

    # Se cambian los labels de campos ya existentes en el formulario
    first_name = forms.CharField(label='Primer Nombre', max_length=150)
    last_name = forms.CharField(label='Primer Apellido', max_length=150)

    # Se hace el correo electrónico como obligatorio, ya que este se usa ahora como nombre de usuario
    email = forms.EmailField(label='Dirección de correo electrónico', required=True)

    class Meta(UserCreationForm.Meta):
        model = User
    
    def save(self, commit=True):
        user = super().save(commit=False)
        # Usamos el correo electrónico como nombre de usuario, ya que el campo 'username' ha sido removido de nuestro formulario personalizado.
        user.username = user.email
        
        # Marcamos al nuevo usuario como administrador (Staff) para que pueda iniciar sesión en el sitio de administración de Django
        user.is_staff = True

        # Construimos el perfil de usuario
        perfil = PerfilDeUsuario(usuario=user, segundo_nombre=self.cleaned_data['segundo_nombre'], segundo_apellido=self.cleaned_data['segundo_apellido'])

        # Django por defecto establece commit como False cuando se usa un UserCreationForm, por lo que el típico:
        # "
        # if commit:
        #     user.save()
        #     perfil.save()
        # "
        # No va a servir.
        # Es por eso que entonces hacemos:
        if not commit:
            user.save()
            perfil.save()

        return user
    
class CustomUserChangeForm(UserChangeForm):
    """
    Formulario de edición de usuarios personalizado para incluir los campos 'Segundo nombre' y 'Segundo Apellido'.
    """

    segundo_nombre = forms.CharField(required=False, max_length=150)
    segundo_apellido = forms.CharField(max_length=150)

    # Se cambian los labels de campos ya existentes en el formulario
    first_name = forms.CharField(label='Primer Nombre', max_length=150)
    last_name = forms.CharField(label='Primer Apellido', max_length=150)

    # Se hace el correo electrónico como obligatorio, ya que este se usa ahora como nombre de usuario
    email = forms.EmailField(label='Dirección de correo electrónico', required=True)

    class Meta(UserCreationForm.Meta):
        model = User
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        try:
            # Obtener el perfil de usuario asociado al usuario actual
            perfil_usuario = PerfilDeUsuario.objects.get(usuario=self.instance)

            # Establecer los valores de los campos del perfil en el formulario
            self.fields['segundo_nombre'].initial = perfil_usuario.segundo_nombre
            self.fields['segundo_apellido'].initial = perfil_usuario.segundo_apellido

        except PerfilDeUsuario.DoesNotExist:
            pass
    
    def save(self, commit=True):
        user = super().save(commit=False)
        # Usamos el correo electrónico como nombre de usuario, ya que el campo 'username' ha sido removido de nuestro formulario personalizado.
        user.username = user.email

        # Editamos los valores del perfil de usuario
        perfil = PerfilDeUsuario.objects.get(usuario = user)
        perfil.segundo_nombre = self.cleaned_data['segundo_nombre']
        perfil.segundo_apellido = self.cleaned_data['segundo_apellido']

        # Al igual que con UserCreationForm, Django por defecto establece commit como False cuando se usa un UserChangeForm, por lo que el típico:
        # "
        # if commit:
        #     user.save()
        #     perfil.save()
        # "
        # Tampoco va a servir aquí.
        # Es por eso que entonces hacemos:
        if not commit:
            user.save()
            perfil.save()
        
        return user



